sap.ui.define(['jquery.sap.global', 'sap/ui/model/ChangeReason', 'sap/ui/model/ContextBinding', './namespace'],
    function (jQuery, ChangeReason, ContextBinding, namespace) {
        'use strict';

        var MobxContextBinding = ContextBinding.extend(namespace + '.MobxContextBinding', {
            constructor: function (mobxModel, path, context, params, events) {
                ContextBinding.apply(this, arguments);

                this._model = mobxModel;
                this._path = path;
                this._context = context;
                this._params = params;
                this._events = events;

                this._mobxDisposer = mobx.reaction(
                    this._model.getProperty.bind(this._model, path, context),
                    this._fireChange.bind(this, {reason: ChangeReason.Change})
                );
            },
            destroy: function () {
                this._mobxDisposer();
                ContextBinding.prototype.destroy.apply(this, arguments);
            },
            getValue: function () {
                return this._model.getProperty(this._path, this._context);
            },
            setValue: function (value) {
                if (this.bSuspended) {
                    return;
                }
                this._model.setProperty(this._path, value, this._context/*, true*/)
            }
        });

        return MobxContextBinding;

    });
